# Simple demo of of the WS2801/SPI-like addressable RGB LED lights.
# Will color all the lights different primary colors.
# Author: Tony DiCola
# License: Public Domain
from __future__ import division
import time

# Import the WS2801 module.
import Adafruit_WS2801
import Adafruit_GPIO.SPI as SPI


# Configure the count of pixels:
PIXEL_COUNT = 32

# Specify a hardware SPI connection on /dev/spidev0.0:
SPI_PORT   = 0
SPI_DEVICE = 0
pixels = Adafruit_WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

# Clear all the pixels to turn them off.
pixels.clear()
pixels.show()  # Make sure to call show() after changing any pixels!


for i in range(PIXEL_COUNT):
    pixels.set_pixel_rgb(i, 0, 255, 0) # Set each pixel to red (255,0,0)
    pixels.show()     # call show() to update the pixels with the colors set above 
    time.sleep(1)


# while True:
#     for i in range(PIXEL_COUNT):
#         pixels.set_pixel_rgb(i, 0, 255, 0) # Set each pixel to red (255,0,0)
#         pixels.show()     # call show() to update the pixels with the colors set above 
#         time.sleep(1)
#     pixels.clear()
